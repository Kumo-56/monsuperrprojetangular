import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CreateTodoComponent} from "./components/create-todo/create-todo.component";
import {ListTodosComponent} from "./components/list-todos/list-todos.component";
import {EditTodoComponent} from "./components/edit-todo/edit-todo.component";
import {FormTodoComponent} from "./components/form-todo/form-todo.component";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {TranslateModule} from "@ngx-translate/core";
import {MatNativeDateModule, MatOptionModule} from "@angular/material/core";
import {MatListModule} from "@angular/material/list";
import {TodosRoutingModule} from "./todos-routing.module";
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {MatIconModule} from "@angular/material/icon";
import {MatExpansionModule} from "@angular/material/expansion";
import { FormGroupTodoComponent } from './components/form-group-todo/form-group-todo.component';



@NgModule({
  declarations: [
    ListTodosComponent,
    CreateTodoComponent,
    EditTodoComponent,
    FormTodoComponent,
    FormGroupTodoComponent
  ],
  imports: [
    TodosRoutingModule,
    // Angular
    CommonModule,
    ReactiveFormsModule,
    // Third party
    // NgxTranslate
    TranslateModule,
    //Material
    MatInputModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatButtonModule,
    MatNativeDateModule,
    MatListModule,
    MatButtonToggleModule,
    MatIconModule,
    MatExpansionModule
  ]
})
export class TodosModule { }
