import { TestBed } from '@angular/core/testing';

import { GroupTodosService } from './group-todos.service';

describe('GroupTodosService', () => {
  let service: GroupTodosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GroupTodosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
