import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, tap} from "rxjs";
import {GroupTodo} from "../models/group-todo";
import {HttpClient} from "@angular/common/http";
import {LoaderService} from "../../core/services/loader.service";
import {TodoService} from "./todo.service";
import {Todo} from "../models/todo";

@Injectable({
  providedIn: 'root'
})
export class GroupTodosService {

  private groups: BehaviorSubject<GroupTodo[]> = new BehaviorSubject<GroupTodo[]>([])

  get groups$(): Observable<GroupTodo[]> {
    return this.groups.asObservable();
  }


  constructor( private http: HttpClient,
               private loaderService: LoaderService,
               private todoService: TodoService) { }



  getGroupById(id: number): Observable<GroupTodo> {
    this.loaderService.setLoadingState(true)
    return this.http.get<GroupTodo>(`http://localhost:3000/groups/${id}`)
      .pipe(
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  getGroups(): Observable<GroupTodo[]> {
    this.loaderService.setLoadingState(true)
    return this.http.get<GroupTodo[]>('http://localhost:3000/groups')
      .pipe(
        tap((data) => this.updateGroupTodos(data)),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }



  addGroup(group: GroupTodo): Observable<GroupTodo> {
    this.loaderService.setLoadingState(true)
    return this.http.post<GroupTodo>('http://localhost:3000/groups', group)
      .pipe(
        tap(data => this.updateGroups([...this.groups.value, data])),
        tap({
          next: () => this.loaderService.setLoadingState(false),
          error: () => this.loaderService.setLoadingState(false)
        })
      );
  }

  private updateGroups(groups: GroupTodo[]) {
    this.groups.next(groups);
  }

  private updateGroupTodos(groupsTodo: GroupTodo[]) {
    this.groups.next(groupsTodo);
  }

}
