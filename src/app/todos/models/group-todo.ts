import {Todo} from "./todo";

export interface GroupTodo {
  id:number;
  name:string;
  todos:Todo[];
}
