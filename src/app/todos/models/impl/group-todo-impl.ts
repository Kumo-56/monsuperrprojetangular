import {Todo} from "../todo";
import {TodoStatus} from "../todo-status";
import {GroupTodo} from "../group-todo";

export class GroupTodoImpl implements GroupTodo {
  id!: number;
  name: string;
  todos!: Todo[];

  constructor(name: string) {
    this.name = name;
  }

  static fromPartial(partial: Partial<GroupTodo>): GroupTodoImpl | null {
    if(partial.name === undefined || partial.name === null) {
      return null;
    }

    const group = new GroupTodoImpl(partial.name);
    if (partial.id !== undefined) {
      group.id = partial.id;
    }

    if (partial.todos !== undefined && partial.todos !== null) {
      group.todos=partial.todos.map(todo=>Object.assign({}, todo));
    }

    return group;
  }
}
