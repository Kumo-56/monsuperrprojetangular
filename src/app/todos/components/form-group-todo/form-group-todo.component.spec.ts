import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormGroupTodoComponent } from './form-group-todo.component';

describe('FormGroupTodoComponent', () => {
  let component: FormGroupTodoComponent;
  let fixture: ComponentFixture<FormGroupTodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormGroupTodoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormGroupTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
