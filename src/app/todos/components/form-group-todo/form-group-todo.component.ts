import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {TodoStatus} from "../../models/todo-status";
import {Todo} from "../../models/todo";
import {GroupTodo} from "../../models/group-todo";
import {Observable} from "rxjs";
import {TodoService} from "../../services/todo.service";

@Component({
  selector: 'app-form-group-todo',
  templateUrl: './form-group-todo.component.html',
  styleUrls: ['./form-group-todo.component.scss']
})
export class FormGroupTodoComponent implements OnInit {

  form!: FormGroup;
  todos$:Observable<Todo[]>|null=null;
  @Input() actionOnGoing: boolean = false;
  @Input() group: GroupTodo | null = null;
  @Output() groupTodoSubmit: EventEmitter<Partial<GroupTodo>> = new EventEmitter<Partial<GroupTodo>>();


  constructor(
    private fb: FormBuilder,
    private todoService: TodoService
  ) {
    this.form = this.fb.group({
      name: this.fb.control(null, [Validators.required]),
      todos: this.fb.control([]),
    });
  }

  ngOnInit(): void {
    this.initForm();
    this.todos$=this.todoService.getTodos();
  }


  submit() {
    if (this.group !== null) {
      this.groupTodoSubmit.emit({
        ...this.form.value,
        id: this.group.id
      });
    } else {
      this.groupTodoSubmit.emit(this.form.value);
    }
  }



  resetForm() {
    this.initForm();
  }

  canSubmit() {
    return !this.actionOnGoing && this.form.valid;
  }

  private initForm() {
    if (this.group !== null) {
      this.form.reset({
        ...this.group
      });
    } else {
      this.form.reset();
    }
  }
}
