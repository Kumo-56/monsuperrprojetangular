import {Component} from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {TodoImpl} from "../../models/impl/todo-impl";
import {Todo} from "../../models/todo";
import {Router} from "@angular/router";
import {GroupTodo} from "../../models/group-todo";
import {GroupTodoImpl} from "../../models/impl/group-todo-impl";
import {GroupTodosService} from "../../services/group-todos.service";

@Component({
  selector: 'app-create-todo',
  template: `
    <mat-accordion class="example-headers-align">
      <mat-expansion-panel [expanded]="step === 0" (opened)="setStep(0)" hideToggle>
        <mat-expansion-panel-header>
          <mat-panel-title>
            Ajout d'un groupe
          </mat-panel-title>
          <mat-panel-description>
            <mat-icon>add</mat-icon>
          </mat-panel-description>
        </mat-expansion-panel-header>

        <app-form-group-todo
          (groupTodoSubmit)="submitFormGroup($event)"
          [actionOnGoing]="creationOnGoing">

        </app-form-group-todo>

      </mat-expansion-panel>

      <mat-expansion-panel [expanded]="step === 1" (opened)="setStep(1)" hideToggle>
        <mat-expansion-panel-header>
          <mat-panel-title>
            Ajout d'une tâche
          </mat-panel-title>
          <mat-panel-description>
            <mat-icon>add</mat-icon>
          </mat-panel-description>
        </mat-expansion-panel-header>

        <app-form-todo
          (todoSubmit)="submit($event)"
          [actionOnGoing]="creationOnGoing"
        ></app-form-todo>

      </mat-expansion-panel>


    </mat-accordion>



  `
})
export class CreateTodoComponent {
  creationOnGoing: boolean = false;
  step = 0;

  setStep(index: number) {
    this.step = index;
  }


  constructor(
    private todoService: TodoService,
    private groupTodoService: GroupTodosService,
    private router: Router
  ) {
  }

  submitFormGroup(partialGroup: Partial<GroupTodo>){
    this.creationOnGoing = true;
    const group = GroupTodoImpl.fromPartial(partialGroup);
    if (group === null) {
      console.error('error');
    } else {
      console.log(group.todos);
      console.log(group.id);
      this.groupTodoService.addGroup(group).subscribe({
        next: (data) => this.afterSubmitGroupFormSuccess(data),
        error: (err) => this.afterSubmitError(err),
        complete: () => console.info('YEAH !!'),
      });
    }
  }

  submit(partialTodo: Partial<Todo>) {
    this.creationOnGoing = true;
    const todo = TodoImpl.fromPartial(partialTodo);
    if (todo === null) {
      console.error('error');
    } else {
      this.todoService.addTodo(todo).subscribe({
        next: (data) => this.afterSubmitSuccess(data),
        error: (err) => this.afterSubmitError(err),
        complete: () => console.info('YEAH !!'),
      });
    }
  }

  private afterSubmitSuccess(data: Todo) {
    this.creationOnGoing = false;
    console.info('SUCCESS !!', data);
    this.router.navigate(['/']).catch(console.error);
  }

  private afterSubmitGroupFormSuccess(data: GroupTodo) {
    this.creationOnGoing = false;
    console.log(data.todos);
    console.info('SUCCESS !!', data);
    this.router.navigate(['/']).catch(console.error);
  }

  private afterSubmitError(err: any) {
    console.error("[afterSubmitError] ERROR !!", err);
    this.creationOnGoing = false;
  }


}
