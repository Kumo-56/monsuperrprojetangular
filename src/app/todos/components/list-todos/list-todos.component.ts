import {Component, OnInit} from '@angular/core';
import {TodoService} from "../../services/todo.service";
import {Observable, switchMap} from "rxjs";
import {Todo} from "../../models/todo";
import {TodoStatus} from "../../models/todo-status";
import {GroupTodo} from "../../models/group-todo";
import {GroupTodosService} from "../../services/group-todos.service";

@Component({
  selector: 'app-list-todos',
  template: `
    <ng-container *ngIf="(groupTodoList$ | async) as groupTodo">

      <div *ngIf="groupTodo.length > 0; else noGroups">
        <div *ngFor="let group of groupTodo">

          <mat-accordion >
            <mat-expansion-panel  [expanded]="step === group.id" (opened)="setStep(group.id)" hideToggle>
              <mat-expansion-panel-header>

                <mat-panel-title>
                  {{group.name}}
                </mat-panel-title>
                <mat-panel-description>
                  <mat-icon>add</mat-icon>
                </mat-panel-description>
            </mat-expansion-panel-header>
              <mat-list-item role="listitem" *ngFor="let todo of group.todos">
                <div>
                <p class="mat-h3">{{todo.description}} {{todo.label}} {{todo.status}} {{todo.limitDate}}</p>
                </div>


              </mat-list-item>
            </mat-expansion-panel>
          </mat-accordion>


        </div>
      </div>

      <ng-template #noGroups>
        <p>There are no todos</p>
      </ng-template>
    </ng-container>
  `,
  styles:[`
    :host {
      width: 100%;
    }
    .mat-expansion-panel {
      width: 100%;
    }
    .mat-mdc-form-field + .mat-mdc-form-field
    {  margin-left: 8px;}`]

})
export class ListTodosComponent implements OnInit {
  groupTodoList$: Observable<GroupTodo[]> = this.groupTodosService.groups$;
  statuses: TodoStatus[] = Object.values(TodoStatus);
  step = 0;

  setStep(index: number) {
    this.step = index;
  }


  constructor(
    private groupTodosService: GroupTodosService,
    private todoService: TodoService
  ) {
  }

  ngOnInit(): void {
   this.todoService.getTodos().subscribe();
   this.groupTodosService.getGroups().subscribe();

  }
/*
  delete(todo: Todo) {
    this.todoService.delete(todo).subscribe();
  }

  updateStatus(todo: Todo, status: TodoStatus): void {
    this.todoService.update({...todo, status}).pipe(
      switchMap(() => this.todoService.getTodos())
    ).subscribe();
  }*/
}
